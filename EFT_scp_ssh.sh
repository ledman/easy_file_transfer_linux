#!/bin/bash

#Easy File Transfer scp/shh
#EFT_scp/ssh was made by Nagi from Razer-WoW team.
#Contact us at http://spectrumwow.com/
#or on TeamSpeak3 address: spectrumwow.com
#Enjoy it




#You only need fill these
USER=                                   #User of the destination
IP=                                     #IP destination transfer
DIRECTORY=                              #Directory path to transfer files
PORT=22                                 #Port of shh destination (Default 22)


#You dont need to make changes from here

C_YELLOW="\E[33m"
C_BLUE="\E[34m"
C_RED="\E[31m"
C_GREEN="\E[32m"
C_NORMAL="\E[0m"
C_REDBOLD="\E[1;31m"
C_YELLOWBOLD="\E[1;33m"

EXIT='false'

echo -e "${C_YELLOW}Welcome to EFT_scp/ssh"
echo -e "Please edit your user, ip, and directory from EFT_scp/ssh"
while [ "$EXIT" == "false" ]
do

        CHOSE=''

        while [ "$CHOSE" != "done" ]
        do
                echo -e "${C_NORMAL}You want to transfer ${C_RED}[D]irectory ${C_NORMAL}or ${C_BLUE}[F]ile? ${C_NORMAL} "; read TRANSFER
                if [ "$TRANSFER" == "f" ]
                then
                        CHOSE='done'
                        TRANSFER='file'
                elif [ "$TRANSFER" == "F" ]
                then
                        CHOSE='done'
                        TRANSFER='file'
                elif [ "$TRANSFER" == "d" ]
                then
                        CHOSE='done'
                        TRANSFER='directory'
                elif [ "$TRANSFER" == "D" ]
                then
                        CHOSE='done'
                        TRANSFER='directory'
                fi
        done

        DESTINATION=$USER"@"$IP":"$DIRECTORY

        if [ "$TRANSFER" == "file" ]
        then
		#!/bin/bash
                echo -e "Please type the ${C_GREEN}path ${C_NORMAL}directory of the ${C_BLUE} File ${C_NORMAL}"; read FILE
                echo -e "${C_YELLOWBOLD}The transfer will start in no time to" $USER"@"$IP "at directory" $DIRECTORY
                sleep 3
		scp -P$PORT $FILE $DESTINATION

        elif [ "$TRANSFER" == "directory" ]
        then
                echo -e "Please type the ${C_GREEN}path ${C_NORMAL}of the ${C_RED}Directory  ${C_NORMAL}"; read FILE
                echo -e "${C_YELLOWBOLD}The transfer will start in no time to" $USER"@"$IP "at directory" $DIRECTORY
                sleep 3
		scp -P$PORT -r $FILE $DESTINATION
        fi

echo -e "${C_NORMAL}You want exit EFT_scp/ssh? ${C_YELLOWBOLD}y${C_NORMAL}/${C_REDBOLD}n ${C_NORMAL}"; read EXITRUE
if [ "$EXITRUE" == "y" ]
then
        EXIT='true'
        echo -e "${C_NORMAL}"
fi

done



